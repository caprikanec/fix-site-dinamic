function selectSampler(){
    var img   = $('.sampler .img img');
    var src   = 'img/sampler/';
    var type  = 'standart';
    var color = 'blue';
    var form  = '3';
    $('.color span').click(function(){
        $('.color tr td span').removeClass('click');
        $(this).hasClass('click') ? $(this).removeClass('click') : $(this).addClass('click');
        type = $(this).parent('td').parent('tr').data('type');
        color = $(this).data('color');
        img.attr( "src", src+type+"/"+color+"/"+form+".png" );
    });
    $('.form div').click(function () {
        $('.form div').removeClass('active');
        $(this).hasClass('active') ? $(this).removeClass('active') : $(this).addClass('active');
        form = $(this).find('img').data('form');
        img.attr( "src", src+type+"/"+color+"/"+form+".png" );
    });
    $('.forms div').click(function () {
        console.log('here');
    });
}
function updatePrice( summ ){
    var currentPrice = $('#price');
    var price = parseInt( currentPrice.text().replace(' ','') ) + parseInt( summ );
    var priceText = String(price).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
    currentPrice.text( priceText + '<em>руб</em>' );
}

$(function(){
    selectSampler();
    var topPos = $('.sampler').offset().top;
    $(window).scroll(function() {
        var top = $(document).scrollTop();
        if (top > topPos) $('.sampler').addClass('fixed');
        else $('.sampler').removeClass('fixed');
    });
});