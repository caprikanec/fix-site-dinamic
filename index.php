﻿<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Изготовление адресных табличек на частный дом на заказ</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name=”viewport” content=”user-scalable=no” />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/fonts.css?ver=1.01" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/style.css?ver=1.02" rel="stylesheet">
    <script src="js/jquery-3.1.1.js"></script>
    <script src="js/jquery.mask.js"></script>
    <script src="js/functions.js?ver=1.02"></script>
</head>
<body>
<div id="all">
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter42982674 = new Ya.Metrika({
                    id:42982674,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/42982674" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "2905169", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div>
<img src="//top-fwz1.mail.ru/counter?id=2905169;js=na" style="border:0;position:absolute;left:-9999px;" alt="" />
</div></noscript>
<!-- //Rating@Mail.ru counter -->

    <!-- Окно звонка -->
    <div class="wrap"></div>
    <div class="call-block">
        <div>
            <form id="zvon">
                <div class="h2zag">Заказать звонок</div>
                <h6>Мы перезвоним в ближайшее время</h6>
	            <!-- Hidden Required Fields -->
	            <input type="hidden" name="project_name" value="Vashadress.su">
	            <input type="hidden" name="admin_email" value="79605231666@yandex.ru">
	            <input id="nameform" type="hidden" name="form_subject" value="Заказ звонка">
	            <!-- END Hidden Required Fields -->
    
    			<input type="hidden" name="форма" value="">
    			<input type="hidden" name="цвет" value="">
    			<input type="hidden" name="текст" value="">
    			<input type="hidden" name="материал" value="">
    			<input type="hidden" name="дополнительно" value="">
    			<input type="hidden" name="цена" value="">
                <!--<span id="obrazec"></span>-->
                <input type="text" placeholder="Фамилия имя" name="Имя">
                <input type="text" placeholder="Ваш телефон" class="phone" name="phone">
                <input type="hidden" name="Источник" value=" <?php echo($_GET['utm_source'])?> ">
                <button class="button">Перезвоните мне</button>
            </form>
        </div>
        <div class="close"></div>
    </div>
	<div class="thanks-block">
		<h2>Спасибо!</h2>
		<p>Ваш заказ оформлен,<br>наш менеджер свяжется с Вами<br>в ближайшее время</p>
        <div class="close"></div>
	</div>
    <!-- // Окно звонка -->

    <!-- Окно списка -->
    <div class="cities-block">
        <ul>
            <li>Выбрать из списка</li>
            <li>Выбрать из списка</li>
            <li>Выбрать из списка</li>
            <li>Выбрать из списка</li>
            <li>Выбрать из списка</li>
        </ul>
    </div>
    <div class="streets-block">
        <ul>
            <li>Выбрать из списка</li>
            <li>Выбрать из списка</li>
            <li>Выбрать из списка</li>
            <li>Выбрать из списка</li>
            <li>Выбрать из списка</li>
        </ul>
    </div>
    <!-- // Окно списка -->

    <header>
            <div class="fix">
                <div class="logo">
                    <a href="index.html"><img src="img/logo.png" alt="Logo"></a>
                    
                </div>
                <div class="schothick">
										<h1 style="font-weight: bold; text-align: center;">Адресные таблички для частного дома</h1>
                    <p style="text-align: center;">по низким ценам от производителя <br>
                    без предоплаты</p>
                </div>
                <div class="address">
                    <div class="h2zag">8 (960) 523 16 66</div>
                    <div class="button">Заказать звонок</div>
                </div>
            </div>
</header>
    <nav>
        <div class="fix">
            <ul>
                <li class="active"><a href="#"><i class="fa fa-home" aria-hidden="true"></i>Главная</a></li>
                <li><a href="delivery.html">Доставка</a></li>
                <li><a href="payment.html">Оплата</a></li>
                <li><a href="question.html">Вопросы</a></li>
                <li><a href="request.html">Отзывы</a></li>
                <li><a href="projects.html">Наши работы</a></li>
                <li><a href="contact.html">Контакты</a></li>
            </ul>
        </div>
    </nav>
    <section class="banner1">
                     <p class="banner_text"><span>Закажите адресную табличку для дома</span><br>
                    со сроком службы  до 15  лет и<br>
                    получите ее уже через 7 дней с  момента заказа!</p>
                 <div class="schothick1">
                       <p class="zgbox">БЕСПЛАТНАЯ ДОСТАВКА</p>
                    <div class="schbox">
                        <p style="font-weight: 300; line-height:27px; color: #424242; text-align: center;">Закажите табличку<br>на сумму<b style="font-weight: bold;"> от 1500 рублей</b><br>
												и доставка будет для Вас <b style="font-weight: bold;">БЕСПЛАТНОЙ</b><s id="rand"></s></p>
                    </div>
                    <div class="eTimer" ></div>
                    <div class="button" id="bsdos" onclick="war()">Заказать с бесплатной доставкой!</div>
                </div>
    </section>
    <main class="main">
        <div class="fix">
            <section class="select-sampler">
				<div class="h3zag1"><i class="h3 h34"></i><span>Что пишем на табличке:</span></div>
				<table class="text-sampler"><tr>
                    <!--<td><span class="after">1</span></td>-->
                    <td><input class="street" type="text"placeholder="улица"></td>
                    <td><input class="namestreet" type="text" placeholder="Название"></td>
                    <td><input class="numberstreet" type="text" placeholder="11"></td>
                </tr></table>
                <h3  id="form_raz"><i class="h3"></i>Выберите размер и форму таблички:</h3>
				<table class="form">
                    <tr>
                        <td rowspan="4">
                            <!--<span class="after">2</span>-->
                            <div class="forms"> 
                                <div class="standard act" data-p="1">
                                    <h6>Стандартный</h6>
                                    <img alt="" src="img/sampler/standard.png" id="standard">
                                    <div class="standard-hover">
                                        <h6>Стандартный</h6>
                                        <p>Подходит для большинства частных домов</p>
                                    </div>
                                </div>
                                <hr>
                                <div id="increased" class="increased" data-p="1.9">
                                    <h6>Увеличенный</h6>
                                    <img alt="" src="img/sampler/increased.png" id="increased">
                                    <div class="increased-hover">
                                        <h6>Увеличенный</h6>
                                        <p>Подходит для  многоквартирных домов или если  дом далеко <br> от дорог</p>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td rowspan="4" class="td-zone">
                        </td>
                        
			<?php require_once ('chuseform.php') ?>
            
                </table>
                <h3><i class="h3 h32"></i>Выберите цвет адресной таблички:</h3>
                <table class="color">
					<tr data-type="standard">
                    	<td colspan="3"><strong class="st">Стандартный</strong></td>
                        <td><span class="click" style="background:#0760c4" data-name="Голубой" data-color="blue"></span></td>
                        <td><span style="background:#051867" data-name="Синий" data-color="darkblue"></span></td>
                        <td><span style="background:#7f1917" data-name="Бордовый" data-color="darkred"></span></td>
                        <td><span style="background:#ff3b1d" data-name="Алый" data-color="red"></span></td>
                        <td><span style="background:#0c6024" data-name="Зеленый" data-color="green"></span></td>
                        <td><span style="background:#443322" data-name="Коричневый" data-color="brown"></span></td>
                    </tr>
                    <tr data-type="light">
                        <td colspan="3">
                            <strong class="st">Световозвращающий</strong>
                            <div class="hint"><b>?</b></div>
                                <div class="hint-box">
                                    <img src="img/hint1.png">
                                    <table>
                                        <tr><td><strong><em>Со световозвращением</em> <em>Стандартная</em></strong> <br><u>(фото сделано со вспышкой в темноте)</u></td></tr>
                                        <tr><td>Если к Вам часто приезжают гости на автомобилях, лучше возьмите знак со светоотражением. При попадании света на такую табличку, она отражает свет в обратном направлении, получается эффект свечения, как у дорожных знаков. Источником света могут быть фары, фонарик или вспышка (как на фото). Ночью такую табличку будет лучше видно.</td></tr>
                                    </table>
                                </div>
                           
                            <br><s>(+ХХХ руб.)</s>
                        </td>
                        <td><span style="background:#024ca5" data-name="Синий" data-color="blue"></span></td>
                        <td><span style="background:#bd1a11" data-name="Красный" data-color="red"></span></td>
                        <td><span style="background:#00732a" data-name="Зеленый" data-color="green"></span></td>
                        <td><span style="background:#c24300" data-name="Оранжевый" data-color="orange"></span></td>
                        <td><span style="background:#ddaf01" data-name="Желтый" data-color="yellow"></span></td>
                        <td><span style="background:#111111" data-name="Черный" data-color="black"></span></td>
                    </tr>
                </table>
                <div class="h3zag"><i class="h3 h33"></i>Выберите материал основы:</div>
                <table class="material"><tr>
                    <td><input type="radio" name="material" class="radio" id="plastik" checked><label for="plastik"><strong>Пластик ПВХ (4мм)</strong></label><!--<span class="after">4</span>-->
                        <div class="hint"><b>?</b></div>
                            <div class="hint-box">
                                <img src="img/hint2.png">
                                <table>
                                    <tr><td>Легкий, прочный и долговечный материал. Не деформируется со временем, не трескается на морозе, и не выгорает на солнце. Из такого же материала делают профили для пластиковых окон. Подойдет в большинстве случаев.</td></tr>
                                </table>
                            </div>
                        
                    </td>
                    <td><input type="radio" name="material" class="radio" id="komposit"><label for="komposit"><strong>Композит (3мм)</strong><s>(+ ХХХ руб.)</s><br><em>ПРОЧНЕЕ</em></label>
                        <div class="hint"><b>?</b></div>
                            <div class="hint-box">
                                <img src="img/hint3.png">
                                <table>
                                    <tr><td>Очень прочный. Состоит из двух листов алюминия и слоя пластика между ними. Используется в строительстве для облицовки фасадов зданий. Подходит в случае повышенных требований к механической прочности таблички, а также если знак будет крепиться на столбик или нетвердую поверхность (например, сетка рабица)</td></tr>
                                </table>
                            </div>
                        
                    </td>
                </tr></table>                
                <div class="h3zag"><i class="h3 h35"></i>Дополнительные параметры:</div>
                <div class="additionally">
                    <h6><strong>Вы также можете выбрать дополнительные параметры:</strong></h6>
                    <div>
                        <div class="add e1"><input type="checkbox" name="material" class="radio" id="lamin"><label for="lamin"><strong>Ламинирование, защита от УФ-лучей</strong>(+ <s>ХХХ</s> руб.)<br><em>+ 5 лет к сроку службы!</em></label>
                            <div class="hint"><b>?</b></div>
                                <div class="hint-box">
                                    <img src="img/hint4.png">
                                    <table>
                                        <tr><td>Мы нанесем на Вашу табличку специальное покрытие, которое защитит изображение от вредного воздействия ультрафиолета и других неблагоприятных внешних факторов, а также от механических повреждений. Это значительно продлит срок службы Вашей таблички</td></tr>
                                    </table>
                                </div>
                            
                        </div>
                        <div class="add e2"><input type="checkbox" name="material" class="radio" id="sverl"><label for="sverl">Сверление отверстий (+ <s>100</s> руб.)</label>
                            <div class="hint"><b>?</b></div>
                                <div class="hint-box">
                                    <img src="img/hint5.png">
                                    <table>
                                        <tr><td><strong><em>Заказать у профессионалов</em> <em>Сделать самому</em></strong></td></tr>
                                        <tr><td><br>Отверстия в табличке можно сделать самому. Пластик сверлится легко, не трескается. Также можно заказать отверстия у нас<br></td></tr>
                                    </table>
                                </div>
                           
                        </div>
                        <div class="add e3"><input type="checkbox" name="material" class="radio" id="dekor"><label for="dekor">Декоративные заглушки-наклейки в цвет таблички (+ 50 руб.)</label>
                            <div class="hint"><b>?</b></div>
                                <div class="hint-box">
                                    <img src="img/hint6.png">
                                    <table>
                                        <tr><td><em>Без наклейки</em> <em>С наклейкой</em></strong></td></tr>
                                    </table>
                                </div>
                            
                        </div>
                    </div>
                    <h6>Комплект креплений (+ 50 руб.):
                        <div class="hint"><b>?</b></div>
                            <div class="hint-box krep">
                                <img src="img/hint7.png">
                            </div>                        
                    </h6>
                    <table id="krepl">
                        <tr>
                            <td><input type="checkbox" name="mount" class="radio" id="brick"><label for="brick">Для кирпича</label></td>
                            <td><input type="checkbox" name="mount" class="radio" id="tree"><label for="tree">Для дерева</label></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="mount" class="radio" id="siding"><label for="siding">Для сайдинга</label></td>
                            <td><input type="checkbox" name="mount" class="radio" id="decking"><label for="decking">Для профнастила</label></td>
                        </tr>
                    </table>
                </div>
            </section>
            <section class="sampler">
                <div class="img"><img alt="" src="img/3.png"><span></span></div>
                <table class="characteristic" width="450px">
                    <tr align="center"><td><i class="sampler-size"></i></td><td> <i class="sampler-colour"></i></td> <td><i class="sampler-basis"></i></td></tr>
                    <tr align="center"><td><strong>Размер:</strong><span> 70х30см<br>(стандатный)</span></td><td><strong>Цвет:</strong><span> Голубой <br>(стандартный)</span></td><td><strong>Основа:</strong><br><span> Пластик ПВХ</span></td></tr>
                    <tr style="display:none"><td><strong>Текст на табличке:</strong></td><td><span class="test1"></span><span class="test2"></span><span class="test3"></span></td></tr>
                    <tr></tr>
                </table>
                <table class="additionally-characteristic">
                    <tr><td><i class="not-added"><span></span></i></td><td>Ламинирование, защита от УФ-лучей</td></tr>
                    <tr><td><i class="not-added"><span></span></i></td><td>Сверление отверстий </td></tr>
                    <tr><td><i class="not-added"><span></span></i></td><td>Декоративные заглушки-наклейки</td></tr>
                    <tr><td><i class="not-added"><span></span></i></td><td>Комплект креплений</td></tr>
                    <tr></tr>
                </table>
                <span id="price"><q>950</q><em>руб</em></span>
                <div class="button add_basket">Быстрый заказ</div>
                <div class="hiden"><div class="button bsk">Оформить заказ</div>или<div class="button button_red">Заказать еще одну со скидкой 10%</div></div>
            </section>
        </div>
    </main>
    <section class="banner banner2">
        <img alt="" src="img/banner1-2.png">
    </section>
    <footer>
        <div class="fix">
            <div class="logo">
                <a href="#"><img src="img/logo1.png" alt="Logo"></a>
            </div>
            <div class="pay">
                <h6>Мы принимаем:</h6>
                <a href="#" class="maestro" title="Maestro"></a>
                <a href="#" class="robokassa"></a>
                <a href="#" class="paypal"></a>
                <a href="#" class="web"></a>
                <a href="#" class="visa"></a>
                <a href="#" class="qiwi"></a>
            </div>
			<div class="socseti">
				<p>мы в соц. сетях:</p>
				<table class="tablesoc">
					<tr>
						<td><a href="https://www.youtube.com/channel/UCYwUyb9ZH2Yn-TXA-iLFyJA" target="_blank"><img src="img/you tube.png"></a></td>
						<td><a href="https://vk.com/znak_na_dom" target="_blank"><img src="img/vk.png"></a></td>
					</tr>
				</table>
			</div>
			<div class="deliv-cont">
            	<p class="delivery">Доставка по всей России!</p>
	            <section class="contact">
	                <i class="fa fa-phone" aria-hidden="true"></i><strong>8 (960) 523 16 66</strong><br>
	                <i class="fa fa-envelope" aria-hidden="true"></i>79605231666@yandex.ru
	            </section>
			</div>
        </div>
    </footer>
</div>
</body>
</html>